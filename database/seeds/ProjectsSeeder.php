<?php

use Illuminate\Database\Seeder;
use App\Project;
use App\Task;


class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //create project "can use factory but time is no to mush and toke some time"
        $project = new Project();
        $project->name = "Project Applicaiton";
        $project->description = "Build an application to manage project associated tasks";
        $project->save();

        //create some tasks

        $task = new Task();
        $task->name = "Create Database using Migrations";
        $task->priority = 1;
        $task->status = 1;

        //save the tasks to project
        $project->Tasks()->save($task);

        //another tasks

        $task = new Task();
        $task->name = "Add some data using db seeding from laralel";
        $task->priority = 2;
        $task->status = 1;

        //save the tasks to project
        $project->Tasks()->save($task);


        //one more project
        $project = new Project();
        $project->name = "Own Video";
        $project->description = "Make a video and explain your experience using php and laravel";
        $project->save();

        //create some tasks

        $task = new Task();
        $task->name = "Talk about your self, experience etc...";
        $task->priority = 1;
        $task->status = 1;

        //save the tasks to project
        $project->Tasks()->save($task);

        //another tasks

        $task = new Task();
        $task->name = "Upload the video";
        $task->priority = 1;
        $task->status = 1;

        //save the tasks to project
        $project->Tasks()->save($task);


        //another tasks
        $task = new Task();
        $task->name = "Check if the upload have been submited";
        $task->priority = 1;
        $task->status = 1;

        //save the tasks to project
        $project->Tasks()->save($task);



    }
}
