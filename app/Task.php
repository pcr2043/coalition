<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [];


    public static function Create(){

        $task = new Task();
        $task->name = '';
        $task->priority = 1;
        $task->status = 0;
       
        return $task;
    }
    //
    public function Project()
    {
        return $this->belongsTo(Project::class);
    }
}
