<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;


class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects.index')->with('projects', Project::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.edit')->with('project', new Project())->with('task', Task::Create());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //start an id var to validate unique names for Project
        $id = -1;

        //if the id is present store the new id and validate if there is not a new task with the same already
        if (isset($request->id))
            $id = $request->id;

        //validate the data
        $validatedData = $request->validate([
            'name' => 'required|unique:projects,id,' . $id,
            'description' => 'required|:min:0|max:250'
        ]);

        //get the instance or new instance
        if ($id != -1)
            $project = Project::find($request->id);
        else
            $project = new Project();

        //fill and save
        $project->fill($validatedData);
        $project->save();

        //rediect the project instance and create an empty model for the task
        return view('projects.edit')->with('project', $project)->with('task', Task::Create());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('projects.edit')->with('project', Project::find($id))->with('task', Task::Create());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::find($id)->delete();
       
    }
}
