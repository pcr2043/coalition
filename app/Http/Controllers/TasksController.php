<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //add session var to determine the workflow of the post,
        //cause we use a single page to manage project and tasks related
        session()->flash('workflow', 'task');
        
        //start an id var to validate unique names for tasks
        $id = -1;
       
        //if the id is present will store the new id validate if there is not a new task with the same already
        if (isset($request->id))
            $id = $request->id;
        
        //validate
        $validatedData = $request->validate([
            'name' => 'required|unique:tasks,id,' . $id,
            'priority' => 'required|:min:1|max:3',
            'status' => 'required|:min:0|max:1',
            'project_id' => 'required|exists:projects,id'

        ]);

        //get the instance or new instance
        if ($id != -1)
            $task = Task::find($request->id);
        else
            $task = new Task();

        
        //fill and save
        $task->fill($validatedData);
        $task->save();


        //rediret to the route of the current project
        return redirect()->route('projects.show', ['id' => $request->project_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Task::find($id)->delete();
    }
}
