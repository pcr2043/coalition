<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [];




    //funciton to retreive the related tasks
    public function Tasks()
    {
        return $this->hasMany(Task::class);
    }
}
