## Project Tasks Management Installation


To install make sure you have the laravel cli installed and create a database called projects

For the laravel cli you can look how to install
<a href="https://laravel.com/docs/5.5#installation">Laravel Installation</a>

Database user and password can be changed on the .env file be looking at DB_USERNAME and DB_PASSWORD, the default is root fot both

after getting the laravel cli installed open a command promp in the applicaiton directory afteand run the follow command:

php artisan migrate -> this will create the tables on the database

php artisan db:seed -> this will seed the database with some projects

then to test the application you shoul start the a php server, for this you should use the laravel cli "it's easy :)"

so run:

php artisan serve

you can specify a port: like 

php artisan serve --port=8000


open the browser in http://localhost:8000/

and you sould able to see the app running..



## How to use the app

The main page lists all project, there a button to create a new project, and in each record you have more two buttons, one for editing the current project and another to remove.

When adding a new project you should fill the name and description and after clicking on the save button the project is created and you shoul now see a another button at bottm to create a task, by clicking on this button a form for the task will be provided. You can fill in same way as the project, there 3 levels for priority "LOW, MEDIUN AND HIGHT" and i add a second field to check is the task is completed or not.

So i hope you enjoy.

any question email me to pcr2043@gmail.com or on skype patrick.cabral.reis

cheers


## developer

please run npm install to install dependencies

to compile assets please run 

npm run watch-poll





