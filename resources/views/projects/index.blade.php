@extends('layout') 
@section('content')
<div class="row">
    <div class="col">
        <h2 class="mb-3">
            <strong>Projects</strong>
            <a role="button" href="{{ url('projects/create') }}" class="btn btn-primary float-right">ADD PROJECT</a>
        </h2>
        <ul class="list-group">
            @foreach($projects as $project)
            <li class="list-group-item">{{ $project->name }}
                <a role="button"  href="{{ url('projects/'.$project->id) }}"  class="btn btn-primary btn-sm float-right"><i class="fa fa-edit"></i></a>
                <button type="button" onclick="remove({{ $project->id }})" class="btn btn-danger btn-sm float-right mr-2"><i class="fa fa-trash"></i></button>
            </li>
            @endforeach
        </ul>
    </div>
</div>




@endsection



@section('scripts')

<script>
function remove(id)
{
    swal({
        title: 'Are you sure about to delete ?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {
        
        axios.delete('projects/' + id).then(response =>{
            
            
               window.location.reload();
           
        })
        .catch(error =>{
           
         
        });

    }).catch(error =>{
        
    })
}
</script>
@stop