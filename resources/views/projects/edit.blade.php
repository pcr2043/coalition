@extends('layout') 
@section('content')
<div class="row">
    <div class="col">
        <h2 class="mb-3">
            <strong>CREATE PROJET </strong>
            <a role="button" href="/projects" class="btn btn-primary float-right">BACK TO  PROJECTS</a>
        </h2>
        <form method="post" action="{{ url('projects') }}">
            {{ csrf_field() }} @if(isset($project->id))
            <input type="hidden" name="id" value="{{ $project->id }}"> @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" value="{{ $project->name }}" class="form-control" placeholder="Type the name of the project">                        @if($errors->has('name') && !session()->has('workflow'))
                        <small id="helpId" class="text-danger">{{ $errors->get('name')[0] }}</small> @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" rows="3" class="form-control" placeholder="Type a description for the project">{{ $project->description }} 
                    </textarea>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary float-right">SAVE</button>
                        <a role="button" href="/projects" class="btn btn-danger float-right mr-2">CANCEL</a>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <hr>
                <br>
                <br>
                <h2 class="mb-3">
                    <strong>Tasks</strong> @if(isset($project->id))
                    <button type="button" onclick="return addTask()" class="btn btn-primary float-right">ADD TASK</button>                    @endif
                </h2>
            </div>
            <div class="col-md-6">
                <ul class="list-group">
                    @foreach($project->tasks as $task)
                    <li class="list-group-item">{{ $task->name }}
                        <button type="button" onclick="return editTask({{  $task->toJson() }})" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button type="button" onclick="removeTask({{ $task->id }})" class="btn btn-danger btn-sm float-right mr-2"><i class="fa fa-trash"></i></button>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6 form-task" style="{{ isset($task->id) && $errors->has('name') ? '' : 'display:none' }}">
                <form method="post" action="{{ url('tasks') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $task->id }}">
                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" name="name" value="{{ $task->name }}" class="form-control" placeholder="Type the name of the project">                                @if($errors->has('name'))
                                <small id="helpId" class="text-danger"> {{ $errors->get('name')[0] }}</small> @endif
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label for="priority" class="w-100">Priority</label>
                                    <label class="radio-inline mr-4">
                                                    <input type="radio" {{ $task->priority == 1 ? 'checked' : '' }} value="1"  class="ml-2" name="priority">
                                                    <span class="mr-2">LOW</span>
                                                </label>
                                    <label class="radio-inline mr-4">
                                                    <input type="radio"  {{ $task->priority == 2 ? 'checked' : '' }}  value="2" class="ml-2" name="priority">
                                                
                                                    <span class="mr-2">MEDIUM</span>
                                                </label>
                                    <label class="radio-inline">
                                                    <input type="radio"  {{ $task->priority == 3 ? 'checked' : '' }}  value="3" class="ml-2" name="priority">
                                                    <span class="mr-2">HIGH</span>
                                                </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label for="status" class="w-100">Status</label>
                                    <label class="radio-inline mr-4">
                                        <input type="radio"  {{ $task->status == 0 ? 'checked' : '' }}  value="0" class="ml-2" name="status">
                                        <span class="mr-2">INCOMPLETE</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio"   {{ $task->status == 1 ? 'checked' : '' }}  value="1" class="ml-2" name="status">
                                         <span class="mr-2">COMPLETE</span>
                                     </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary float-right">SAVE</button>
                                <a role="button" href="/tasks" class="btn btn-danger float-right mr-2">CANCEL</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('scripts')
<script>
    function addTask(){
    $(".form-task").toggle();
    return false;   
}
function editTask(task)
{
    $(".form-task").find("[name='id']").val(task.id).change();
    $(".form-task").find("[name='project_id']").val(task.project_id).change();
    $(".form-task").find("[name='name']").val(task.name).change();
    $(".form-task [name='priority'][value="+ task.priority+"]").prop('checked', true).change()
    $(".form-task [name='status'][value="+ task.status+"]").prop('checked', true).change()
    $(".form-task").show();
}
function clearTaskForm(){
    
    $(".form-task").find("[name='id']").val('').change();

    $(".form-task").find("[name='name']").val('').change();
    $(".form-task [name='priority']").first().prop('checked', true).change()
    $(".form-task [name='status']").first().prop('checked', true).change()
}

function removeTask(id){
    swal({
        title: 'Are you sure about to delete this Task ?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function () {
        
        axios.delete('/tasks/' + id).then(response =>{
            
            
               window.location.reload();
           
        })
        .catch(error =>{
           
         
        });
    }).catch(error =>{
        
    })
}
</script>
@if(isset($task->id) && !$errors->any())
<script>
    clearTaskForm();
</script>
@endif 
@stop