<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token()}}">
	<title>Coalition - teste</title>
	{{-- STYLES --}}
	<link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body style="display:none">
	<div id="app">
		<app inline-template>
			<div class="app">
				{{-- MAIN --}}
				<section class="main">
					<div class="container">
						@yield('content')
					</div>
				</section>
			</div>
		</app>
	</div>
</body>
{{-- SCRIPTS --}}
<script type="text/javascript" src="/js/app.js"></script>
@yield('scripts')
</html>