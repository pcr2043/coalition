export class Table{

	constructor(fields, endPoint){
		this.fields = fields;
		this.model = { id :-1, loading:false};

		this.sorts =[];
		this.setSorts();
		this.params = [];
		this.params.origin= location.pathname;
		this.params.search= '';
		this.params.active= '';
		this.endpoint = endPoint;
		this.paginate=
		{
			data:[]
		};	

	}

	load(row, value)
	{
		row.loading = value;
	}
	saving(value)
	{
		this.model.loading = value;
	}

	setSorts(){

		this.fields.forEach(field =>{
			let sort = {};
			this.sorts[field]= '';
		})

	}

	setSort(key)
	{	
		let sort = this.sorts[key];

		if(sort == '')
			sort = 'asc';
		else if(sort == 'asc')
			sort = 'desc';
		else if(sort== 'desc')
			sort = '';

		this.sorts[key] = sort;

		this.bind()
	}

	getParams()
	{	
		let params = '?';
		let sortsEnable = [];

		//bind all params filters to params
		Object.keys(this.params).forEach(key =>{
			params += key+ "=" + encodeURI(this.params[key])+'&';
		})

		//set sort to the params

		let sorts = '';

		Object.keys(this.sorts).forEach(key =>{

			if(this.sorts[key].length > 0)
			{
				
				sorts+="sorts[" + key +"]="+this.sorts[key]+"&";

			}
		})

		return  params +'&' + sorts; 
	}

	pages(){
		if(this.paginate.last_page > 2)
		{	
			if(this.paginate.current_page > 1 && this.paginate.current_page < this.paginate.last_page)
				return [this.paginate.current_page -1, this.paginate.current_page];
			else if(this.paginate.current_page == 1)
				return [this.paginate.current_page , this.paginate.current_page +1];
			else
				return [this.paginate.current_page -2, this.paginate.current_page -1];
		}
		else
		{
			let pages =  Array(this.paginate.last_page).fill().map((x, i) => i +1);
			return pages;
		}

	}

	bind(){

		let api = this.endpoint +  this.getParams();
		if(this.endpoint.indexOf('?') > 0)
			api = this.endpoint +  this.getParams().replace('?','&');

		axios.get(api).then(response =>{
			this.paginate = response.data;

			if(this.paginate.data.length == 0)
			{
				if(this.paginate.current_page > 1)
					this.prev();
			}
		}).catch(ex =>{
			console.log("error loading data");
		});
	}
	goto(page){
		this.endpoint= this.paginate.path + '?page=' + page;
		this.bind();
	}
	first(){
		if(this.paginate.links != undefined && this.paginate.links.first != null)
		{
			this.endpoint= this.paginate.links.first;
			this.bind();
		}
		else if(this.paginate.first_page_url != null)
		{

			this.endpoint= this.paginate.first_page_url;
			this.bind();
		}
	}

	prev(){

		if(this.paginate.links != undefined && this.paginate.links.prev != null)
		{
			this.endpoint= this.paginate.links.prev;
			this.bind();
		}
		else if(this.paginate.prev_page_url != null)
		{
			this.endpoint = this.paginate.prev_page_url;
			this.bind();
		}

	}


	next(){

		if(this.paginate.links != undefined &&this.paginate.links.next != null)
		{
			this.endpoint= this.paginate.links.next;
			this.bind();
		}
		else if(this.paginate.next_page_url != null)
		{
			this.endpoint = this.paginate.next_page_url;
			this.bind();
		}
	}

	last(){

		if(this.paginate.links != undefined && this.paginate.links.last != null)
		{
			this.endpoint= this.paginate.links.last;
			this.bind();
		}
		else if(this.paginate.last_page_url != null)
		{

			this.endpoint= this.paginate.last_page_url;
			this.bind();
		}

	}


}