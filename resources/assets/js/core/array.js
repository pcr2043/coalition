export class Arr {

	constructor() {
		this.items = [];
	}

	add(item) {
		this.item.push(item);
	}

	addOrRemove(field, key) {

		if (this.hasKey(field, key)) {
			let foundIndex = null;
			field.forEach((value, index) => {
				if (value == key)
					foundIndex = index;
			});

			field = field.splice(foundIndex, 1);
		}
		else
			field.push(key);

		console.log(field);
	}

	hasKey(field, key) {


		return (field.filter(value => { return value == key }).length > 0)

	}

	addUnique(model, key = null) {


		let item = this.items.filter((itemFiltered, index) => {
			if (key == null)
				return model == itemFiltered;
			else
				return model[key] == itemFiltered[key];
		});
		console.log(item)

		if (item.length == 0)
			this.items.push(model);
	}

	remove(value, key = null) {
		let foundIndex = null;

		this.items.forEach((itemFiltered, index) => {
			if (key == null && itemFiltered == value)
				foundIndex = index;
			else if (itemFiltered[key] == value)
				foundIndex = index;
		});

		if (foundIndex != null)
			this.items.splice(foundIndex, 1);

	}

	clear() {

		this.items = [];
	}

}