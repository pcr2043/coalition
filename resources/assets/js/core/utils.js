export class Utils {

    dateTimeFormatMonthName(date) {

        return moment(date).format("DD MMM YYYY  HH:mm");
    }

    dateTimeFormatMonthYearOnly(date) {

        return moment(date).format("MMM YYYY");
    }

    getDateDiffNow(date) {
        var dateDiff = moment.duration(moment(date).diff(moment.now()));

        let days = Math.abs(dateDiff.asDays());
        let hours = Math.abs(dateDiff.asHours());
        let minutes = Math.abs(dateDiff.asMinutes());
        let seconds = Math.abs(dateDiff.asSeconds());

        if (days >= 1)
            return parseInt(days) + ' days ago';

        if (hours >= 1)
            return parseInt(hours) + ' hours ago';

        if (minutes >= 1)
            return parseInt(minutes) + ' minutes ago';

        if (seconds >= 1)
            return parseInt(seconds) + ' seconds ago';
    }
}